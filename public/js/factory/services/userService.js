app.factory('$userService', function ($rootScope, $http, $httpParamSerializer) {
    var service = {
        action: {},
        data: {}
    };

    //data
    service.data.insertUser = function (name, email, password, confirmPassword) {
        return {
            name: name || "",
            email: email || "",
            password: password || "",
            confirmPassword: confirmPassword || ""
        }
    }
    //action

    service.action.listuser = function () {
        var url = siteUrl + '/User/listuser';
        return $http.get(url);
    }
    service.action.insertUser = function (data) {
        var url = siteUrl + '/User/create';
        return $http.post(url, data);
    }
    service.action.deleteUser = function (id) {
        var url = siteUrl + '/User/delete/' + id;
        return $http.delete(url);
    }
    service.action.updateUser = function (id) {
        var url = siteUrl + '/User/edit/' + id;
        return $http.post(url);
    }


    return service;
});
