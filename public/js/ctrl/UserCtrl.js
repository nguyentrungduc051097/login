app.controller('UserCtrl', function ($scope, $http, $userService) {
    $scope.dataUser = [];
    $scope.currentId = null;
    $scope.data = {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        listRole: "",
        getlistUser: function () {
            $userService.action.listuser().then(function (response) {
                $scope.dataUser = response.data;
            }).catch(function (err) {
                console.log(err);
            })
            // $http.get('http://localhost/project/User/listuser').then(function (response){
            //     console.log(response);
            //     $scope.dataUser = response.data;
            // }).catch(function (err) {
            //     console.log(err)
            // });
        }
    };
    $scope.action = {
        addUser: function () {
            //param

            var param = $userService.data.insertUser($scope.data.name, $scope.data.email, $scope.data.password, $scope.data.confirmPassword,);
            $userService.action.insertUser(param).then(function (resp) {
                // console.log(resp)
            }).catch(function (err) {
                console.log(err)
            })
            // console.log($scope.data.name);
        },
        deleteUser: function (id) {
            $scope.currentId = id;

            console.log("Id:", id);
        },
        confirmDeleteUser: function () {
            console.log("Confirm:", $scope.currentId);
            $userService.action.deleteUser($scope.currentId).then(function (resp) {
                console.log(resp)
            }).catch(function (err) {
                console.log(err)
            })
            // console.log($scope.action.id);
        }
        ,
        updateUser: function (id) {
            $scope.currentId = id;
            console.log("Id:", id);

        },
        confirmUpdateUser: function () {
            // console.log("Confirm:", $scope.currentId);
            // $userService.action.updateUser($scope.currentId,$scope.data.email,$scope.data.name).then(function (resp) {
            //     console.log(resp)
            // }).catch(function (err) {
            //     // console.log(err)
            // });
            var param = $userService.action.updateUser($scope.data.name, $scope.data.email, $scope.currentId);
            $userService.action.updateUser(param).then(function (resp) {
                console.log(resp)
            }).catch(function (err) {
                console.log(err)
            })
            console.log($scope.data.name)

        }


    }

    $scope.data.getlistUser();
});
