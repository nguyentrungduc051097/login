@extends('layouts.app')
@section('content')
    <form action="" method="post">
        @csrf
    <div id="content" class="container-fluid">
        <div class="card">
            <div class="card-header font-weight-bold">
                Thêm người dùng
            </div>
            <div class="card-body">
                <form action="{{url('admin/user/store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Họ và tên</label>
                        <input class="form-control" type="text" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="text" name="email" id="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Mật khẩu</label>
                        <input class="form-control" type="password" name="password" id="email">
                    </div>
                    <div class="form-group">
                        <label for="password-confirm">Xác nhận mật khẩu </label>
                        <input class="form-control" type="password" name="password_confirm" id="email">
                    </div>

                    <div class="form-group">
                        <label for="">Nhóm quyền</label>
                        <select class="form-control" id="" name="roles[]" multiple="multiple">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->display_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary" name="btn_add" value="Thêm mới">Thêm mới</button>
                </form>
            </div>
        </div>
    </div>
    </form>

@endsection
