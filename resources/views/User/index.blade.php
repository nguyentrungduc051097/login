@extends('layouts.app')

@section('myJs')
    <script src="{{ asset('js/factory/services/userService.js') }}"></script>
    <script src="{{ asset('js/ctrl/UserCtrl.js') }}"></script>
@endsection

@section('content')

    <div class="table" ng-controller="UserCtrl">
        <table class="table">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                    ng-click="addListUser('add')">
                Thêm mới
            </button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true" aria-inval id="addModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Thêm</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <form action="{{url('admin/user/store')}}" method="post" id="editForm">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Họ và tên</label>
                                        <input class="form-control" type="text" name="name" id="name"
                                               ng-model="data.name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" type="text" name="email" id="email"
                                               ng-model="data.email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Mật khẩu</label>
                                        <input class="form-control" type="password" name="password" id="email"
                                               ng-model="data.password">
                                    </div>
                                    <div class="form-group">
                                        <label for="password-confirm">Xác nhận mật khẩu </label>
                                        <input class="form-control" type="password" name="password_confirm" id="email"
                                               ng-model="data.confirmPassword">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Nhóm quyền</label>
                                        <select class="form-control" id="" name="roles[]" multiple="multiple">

                                            <option value=""></option>

                                        </select>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" ng-click="action.addUser()">Lưu thông tin
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <thead class="thead-dark">
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Ngày tạo</th>
                <th scope="col">Tác vụ</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="listUser in dataUser">
                <td scope="row"><% listUser.id %></td>
                <td><% listUser.name %></td>
                <td><% listUser.email %></td>
                <td><% listUser.created_at %></td>
                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit"
                            ng-click="action.updateListUser('listUser.id')">
                        Sửa
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#delete"
                            ng-click="action.deleteUser(listUser.id)">
                        xóa
                    </button>
                </td>
            </tr>
            </tbody>
        </table>


        <!-- Modal Delete-->
        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true" aria-inval id="addModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Xóa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <form action="" method="post" id="editForm">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="modal-header bg-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Bấm xóa để hoàn tất xóa dữ liệu!</h4>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" ng-click="action.confirmDeleteUser()">Xóa
                        </button>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Edits-->
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true" aria-inval id="addModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sửa thông tin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <form action="{{url('admin/user/store')}}" method="post" id="editForm">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Họ và tên</label>
                                    <input class="form-control" type="text" name="name" id="name"
                                           ng-model="update.name">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" type="text" name="email" id="email"
                                           ng-model="update.email">
                                </div>
                                <div class="form-group">
                                    <label for="">Nhóm quyền</label>
                                    <select class="form-control" id="" name="roles[]" multiple="multiple">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" ng-click ="action.confirmUpdateUser()">Sửa
                        </button>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection


