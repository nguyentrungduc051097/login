@extends('layouts.app')
@section('content')
    <table class="table">
        <a  class="btn-dark" href="{{route('role.add')}}">Thêm mới</a>
        <thead class="thead-dark">
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Name</th>
            <th scope="col">display_name</th>
            <th scope="col">action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($listRole as $item)
            <tr>
                <th scope="row">{{$loop->index +1}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->display_name}}</td>
                <td>
                    <a class="btn-dark" href="{{route('role.edit',['id'=>$item->id])}}">Edit</a>
                    <a class="btn-dark" href="{{route('role.delete',['id'=>$item->id])}}">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection



