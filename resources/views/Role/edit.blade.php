@extends('layouts.app')
@section('content')
    <form action="" method="post">
        @csrf
        <div id="content" class="container-fluid">
            <div class="card">
                <div class="card-header font-weight-bold">
                    Thêm
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Họ và tên</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{$role->name}}">
                        </div>
                        <div class="form-group">
                            <label for="displayname">Display name</label>
                            <input class="form-control" type="text" name="display_name" id="email" value="{{$role->display_name}}">
                        </div>
                        @foreach($premissions as $premission)
                            <div class="form-check">
                                <input {{$getAllOfPremissionRole->contains('premission_id',$premission->id) ? 'checked' : ''}} type="checkbox" class="form-check-input" id="exampleCheck1" name="premissions[]" value="{{$premission->id}}">
                                <label class="form-check-label">{{$premission->display_name}}</label>
                            </div>
                        @endforeach
                        <button type="submit" class="btn btn-primary" name="btn_add" value="Thêm mới">Thêm mới</button>
                    </form>
                </div>
            </div>
        </div>
    </form>

@endsection
