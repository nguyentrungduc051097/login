<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model{
    protected $fillable = ['name','display_name'];
    public function premissions(){
            return $this->belongsToMany(Premission::class,'role_premissions');
    }

}
