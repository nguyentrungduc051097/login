<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserCtrl extends Controller
{
    private $user;
    private $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    public function getListUser()
    {
        $listUser = $this->user->all();
        return response()->json($listUser);
    }

    public function index()
    {
        $listUser = $this->user->all();
        return view('User.index', compact('listUser'));
        return response()->json($listUser);
    }

    public function create()
    {
        $roles = $this->role->all();
        return view('User.add', compact('roles'));
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            // insert data to user table
            $userCreate = $this->user->create([ // nhận lại đối tượng sau insert
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            //insert data to role_user
//            $userCreate->roles()->attach($request->roles);
//            $roles = $request->roles;
//            foreach ($roles as $roleId){
//                DB::table('role_user')->insert([
//                    'user_id'=>$userCreate->id,
//                    'role_id'=>$roleId
//                ]);
//            }

            DB::commit();
            return redirect()->route('user.index');
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    // Show form edit
    public function edit($id)
    {
        $roles = $this->role->all();
        $user = $this->user->findOrfail($id);
        $listRoleUser = DB::table('role_user')->where('user_id', $id)->get();
//        dd($listRoleUser);
        return view('User.edit', compact('roles', 'user', 'listRoleUser'));
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            //update
            $this->user->where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email
            ]);
            //update role_user
            DB::table('role_user')->where('user_id', $id)->delete();
            $userCreate = $this->user->find($id);
            $userCreate->roles()->attach($request->roles);
            DB::commit();
            return redirect()->route('user.index');
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            //delete_user
            $user = $this->user->find($id);
            $user->delete();
            //delete_user_role
            $user->roles()->detach();
            DB::commit();
            return redirect()->route('user.index');
        } catch (\Exception $exception) {
            DB::rollBack();
        }
        return true;
    }

}
