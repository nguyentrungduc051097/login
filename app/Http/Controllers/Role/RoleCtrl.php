<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use App\Premission;
use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\DB;

class RoleCtrl extends Controller{
    private $role;
    private $premission;
    public function __construct(Role $role, Premission $premission){
        $this->role = $role;
        $this->premission= $premission;
    }
    public function index(){
        $listRole=$this->role->all();
        return view('Role.index',compact('listRole'));
    }
    //show form create role
    public function create(){
        $premissions = $this->premission->all();
        return view('Role.add',compact('premissions'));
    }
    public function store(Request $request){
        try {
            DB::beginTransaction();
            // insert data to role table
            $roleCreate = $this->role->create([ // nhận lại đối tượng sau insert
                'name' => $request->name,
                'display_name' => $request->display_name,
            ]);
            //insert data to role_premission
            $roleCreate->premissions()->attach($request->premission);
            DB::commit();
            return redirect()->route('role.index');
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }
    //show form edit
    public function edit($id){
        $premissions = $this->premission->all();
        $role=$this->role->findOrfail($id);
        $getAllOfPremissionRole= DB::table('role_premissions')->where('role_id',$id)->get();
//        dd($getAllOfPremissionRole);
        return view('Role.edit',compact('premissions','role','getAllOfPremissionRole'));
    }
    // update
    public function update(Request $request,$id){
    try {
        DB::beginTransaction();
        //update
        $this->role->where('id', $id)->update([
            'name' => $request->name,
            'display_name' => $request->display_name
        ]);
        //update role_premission
        DB::table('role_premissions')->where('role_id', $id)->delete();
        $roleCreate = $this->role->find($id);
        $roleCreate->premissions()->attach($request->premission);
        DB::commit();
        return redirect()->route('role.index');
    } catch (\Exception $exception) {
        DB::rollBack();
    }
    }
    //delete
    public function delete(Request $request,$id){
        try {
            DB::beginTransaction();
            //delete_role
            $role=$this->role->find($id);
            $role->delete();
            //delete
            $role->premissions()->detach();
            DB::commit();
            return redirect()->route('role.index');
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }


}

