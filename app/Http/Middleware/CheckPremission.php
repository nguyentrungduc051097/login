<?php

namespace App\Http\Middleware;

use App\Premission;
use App\User;
use Closure;
use Illuminate\Support\Facades\DB;

class CheckPremission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$premission = null){
        // Lấy thông tin các quyền khi login vào hệ thống
        //1. Lấy tất cả các role của user
//        $listRoleUser = DB::table('users')
//            ->join('role_user', 'users.id', '=', 'role_user.user_id')
//            ->join('roles', 'role_user.role_id', '=', 'roles.id')
//            ->where('users.id',auth()->id())
//            ->select('roles.*')
//            ->get()->pluck('id')->toArray();
        $listRoleUser = User::find(auth()->id())->roles()->select('roles.id')->pluck('id')->toArray();
//        dd($listRoleUser);
        //2. lấy ra tất cả quyền khi login vào hệ thống
        $listRoleUser = DB::table('roles')
            ->join('role_premissions', 'roles.id', '=', 'role_premissions.role_id')
            ->join('premissions', 'role_premissions.premission_id', '=', 'premissions.id')
            ->whereIn('roles.id',$listRoleUser)
            ->select('premissions.*')
            ->get()->pluck('id')->unique();
//        dd($listRoleUser);
        // lấy ra mã màn hình tương ứng để check phân quyền
        $checkPremission=Premission::where('name',$premission)->first()->value('id');
//        dd($checkPremission);
        //Kiểm tra xem có được phép vào màn hình hay không
//        if ($listRoleUser->contains($checkPremission)){
            return $next($request);
//        }
//        return abort(401);
    }
}
