<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware('auth')->group(function () {
    //modelUser
    Route::prefix('User')->group(function () {
        //listUser
        Route::get('/user', [
            'as' => 'user.index',
            'uses' => 'User\UserCtrl@index',
            'middleware' => 'checkacl:user-list'
        ]);
        //createUser
        Route::get('/create', 'User\UserCtrl@create')->name('user.add');
        Route::post('/create', 'User\UserCtrl@store')->name('user.store');
        //edit
        Route::get('/edit/{id}', 'User\UserCtrl@edit')->name('user.edit');
        Route::post('/edit/{id}', 'User\UserCtrl@update')->name('user.edit');
        //delete
        Route::delete('/delete/{id}', 'User\UserCtrl@delete')->name('user.delete');
        Route::get('/listuser', 'User\UserCtrl@getListUser');
    });
    //modelRole
    Route::prefix('Role')->group(function () {
        //listUser
        Route::get('/role', 'Role\RoleCtrl@index')->name('role.index');
        //createUser
        Route::get('/role/create', 'Role\RoleCtrl@create')->name('role.add');
        Route::post('/role/create', 'Role\RoleCtrl@store')->name('role.store');
        //edit
        Route::get('/role/edit/{id}', 'Role\RoleCtrl@edit')->name('role.edit');
        Route::post('/role/edit/{id}', 'Role\RoleCtrl@update')->name('role.edit');
        //delete
        Route::get('/role/delete/{id}', 'Role\RoleCtrl@delete')->name('role.delete');
    });
});

//    render blade
Route::get('list', 'RenderCtrl@renderUser');
